
import gulp from 'gulp';
import babel from 'gulp-babel';
import watch from 'gulp-watch';
import screeps from 'gulp-screeps';
import credentials from './credentials.js';

const paths = {
  src: 'src',
  dest: 'build'
};


gulp.task('default', () => {
  return gulp.src(paths.src + "/*.js")
      .pipe(babel())
      .pipe(gulp.dest(paths.dest));
});


gulp.task('watch', () => {
  return watch('src/**/*.js', { ignoreInitial: false })
      .pipe(babel())
      .pipe(gulp.dest(paths.dest));
});



gulp.task('commit', () => {
  return gulp.src(paths.dest + "/*.js")
      .pipe(screeps(credentials));
});
