var settings = {
    maxCreeps: 10,
    creepBuilds: {
        harvester: {
            weight: 0.4,
            body: [WORK,WORK,CARRY,MOVE],
            memory: {
                role: 'harvester',
            }
        },
        upgrader: {
            weight: 0.2,
            body: [WORK,WORK,CARRY,MOVE],
            memory: {
                role: 'upgrader',
            }
        },
        builder: {
            weight: 0.4,
            body: [WORK,WORK,CARRY,MOVE],
            memory: {
                role: 'builder',
                roadDesigner: true
            }
        },
    },

    allies: [
        "Carl",
        "Lukaaaaaaaay"
    ],

};

module.exports = settings;