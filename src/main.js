
import settings from './settings';

import roleHarvester from './role.harvester';
import roleUpgrader  from './role.upgrader';
import roleBuilder   from './role.builder';

module.exports.loop = function () {

    var tower = Game.getObjectById('TOWER_ID');
    if(tower) {
        var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (structure) => structure.hits < structure.hitsMax
        });
        if(closestDamagedStructure) {
            tower.repair(closestDamagedStructure);
        }

        var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if(closestHostile) {
            tower.attack(closestHostile);
        }
    }

    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }

    var creepCount = {},
        creepTotal = _.keys(Game.creeps).length;

    for(var type in settings.creepBuilds) {
        creepCount[type] = _.filter(Game.creeps, (creep) => creep.memory.role == settings.creepBuilds[type].memory.role).length;
    }

    var freeEnergy = Game.spawns['Spawn1'].room.energyAvailable;
    if (creepTotal < settings.maxCreeps && freeEnergy >= 300) {
        for(var type in settings.creepBuilds) {
            console.log(type + " weight: " + (creepCount[type] / creepTotal) + " target: " + settings.creepBuilds[type].weight);
            if (creepCount[type] / creepTotal < settings.creepBuilds[type].weight) {
                var newName = Game.spawns['Spawn1'].createCreep(settings.creepBuilds[type].body, undefined, settings.creepBuilds[type].memory);
                console.log("Spawning new " + type + " '" + newName + "'");
                break;
            }

        }
    }

    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if(creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
    }
}