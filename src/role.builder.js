import helper from './helper';

var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {

        // if (creep.memory.roadDesigner && !creep.pos.lookFor(STRUCTURE_ROAD).length) {
        //     creep.pos.createConstructionSite(STRUCTURE_ROAD);
        // }


        if(creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
            delete creep.memory.buildTargetId;
            creep.say('WORK IT');
        }
        if(!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
            creep.memory.building = true;
            creep.say('MAKE IT');
        }

        if(creep.memory.building) {
            if (!creep.memory.buildTargetId) {
                var constructionSites = creep.room.find(FIND_CONSTRUCTION_SITES);
                creep.memory.buildTargetId = helper.closest(creep, constructionSites).id;
            }

            var target = Game.getObjectById(creep.memory.buildTargetId),
                buildAttempt = creep.build(target);

            switch (buildAttempt) {
                case OK:
                    creep.say("DO IT");
                    break;

                case ERR_NOT_IN_RANGE:
                    creep.moveTo(target);
                    break;

                case ERR_INVALID_TARGET:
                    var constructionSites = creep.room.find(FIND_CONSTRUCTION_SITES);
                    creep.memory.buildTargetId = constructionSites[constructionSites.length - 1].id;
                    break;

                default:
                    console.log("Unknown build response: " + buildAttempt);
                    break;
            }
        }
        else {
            var fullSpawns = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return structure.structureType == STRUCTURE_SPAWN && structure.energy == structure.energyCapacity;
                }
            });

            if(fullSpawns.length > 0) {
                creep.say('stealing energy from spawn LOL');
                if(fullSpawns[0].transferEnergy(creep, creep.carryCapacity) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(fullSpawns[0]);
                }
            } else {
                helper.targetClosestSource(creep);
            }
        }
    }
};

module.exports = roleBuilder;