var helper = {

    /** @param {Creep} creep **/
    closest: function(entity, candidates) {
        var lowestDistance,
            closest;

        _.forEach(candidates, function(structure) {
            var candidateDistance = this.lineDistance(entity.pos, structure.pos);

            if (typeof lowestDistance == "undefined" || candidateDistance < lowestDistance) {
                lowestDistance = candidateDistance;
                closest = structure;
            }
        }.bind(this));
        return closest;
    },

    lineDistance: function (point1, point2) {
        var xs = point2.x - point1.x,
            ys = point2.y - point1.y;

        xs = xs * xs;
        ys = ys * ys;

        return Math.sqrt( xs + ys );
    },

    targetClosestSource: function(creep) {
        var target;

        if (creep.memory.targetId) {
            target = Game.getObjectById(creep.memory.targetId);
        }

        if (typeof target == "undefined" || target.amount == 0) {
            var sources = creep.room.find(FIND_SOURCES),
                idealSource;

            creep.say('finding new source');

            idealSource = this.closest(creep, sources);

            if (idealSource) {
                creep.memory.targetId = idealSource.id;
            }
        }

        if (!target && creep.memory.targetId) {
            target = Game.getObjectById(creep.memory.targetId);
        }

        if(creep.harvest(target) == ERR_NOT_IN_RANGE) {
            creep.moveTo(target);
        }
    }
};

module.exports = helper;