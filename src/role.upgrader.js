import helper from './helper';

var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {

        if(creep.memory.upgrading && creep.carry.energy == 0) {
            creep.memory.upgrading = false;
            creep.say('harvesting');
        }
        if(!creep.memory.upgrading && creep.carry.energy == creep.carryCapacity) {
            creep.memory.upgrading = true;
            creep.say('upgrading');
        }

        if(creep.memory.upgrading) {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            }
        }
        else {
            var fullSpawns = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return structure.structureType == STRUCTURE_SPAWN && structure.energy == structure.energyCapacity;
                }
            });

            if(fullSpawns.length > 0) {
                creep.say('stealing energy from spawn LOL');
                if(fullSpawns[0].transferEnergy(creep, creep.carryCapacity) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(fullSpawns[0]);
                }
            } else {
                helper.targetClosestSource(creep);
            }
        }
    }
};

module.exports = roleUpgrader;