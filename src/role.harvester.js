import helper from './helper';

var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {


        if(creep.carry.energy < creep.carryCapacity) {
            this.findSource(creep);
        }
        else {
            var targets = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return structure.structureType == STRUCTURE_SPAWN && structure.energy < structure.energyCapacity;
                    }
            });

            if(targets.length > 0) {
                if(creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            } else {
                // Find another non-harvester creep that needs energy
                var closestWorkerCreep = helper.closest(creep, _.filter(Game.creeps, (creep) => creep.memory.role != "harvester"));
                creep.say('finding a friend!');
                if(creep.transfer(closestWorkerCreep, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(closestWorkerCreep);
                }
            }
        }
    },

    findSource: function(creep) {
        var target;

        if (creep.memory.targetId) {
            target = Game.getObjectById(creep.memory.targetId);
        }


        if (typeof target == "undefined" || target.amount == 0) {
            var sources = creep.room.find(FIND_SOURCES),
                idealSource,
                maxEnergy = -1;

            creep.say('finding new source');


            //idealSource = helper.closest(creep, sources);
            idealSource = _.sample(sources);

            if (idealSource) {
                creep.memory.targetId = idealSource.id;
            }
        }

        if (!target && creep.memory.targetId) {
            target = Game.getObjectById(creep.memory.targetId);
        }

        if(creep.harvest(target) == ERR_NOT_IN_RANGE) {
            creep.moveTo(target);
        }
    }
};

module.exports = roleHarvester;